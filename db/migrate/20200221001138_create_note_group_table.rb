class CreateNoteGroupTable < ActiveRecord::Migration[6.0]
  def change
    create_table :note_groups do |t|
      t.integer :note_id
      t.integer :group_id
    end
  end
end
