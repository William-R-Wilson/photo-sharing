class AddVideoToEvent < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :video, :string
  end
end
