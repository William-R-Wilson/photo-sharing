class CreateEventImages < ActiveRecord::Migration[6.0]
  def change
    create_table :event_images do |t|
      t.integer :event_id
      t.integer :image_id

      t.timestamps
    end
  end
end
