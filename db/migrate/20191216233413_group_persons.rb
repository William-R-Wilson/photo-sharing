class GroupPersons < ActiveRecord::Migration[6.0]
  def change
    create_table :group_persons do |t|
      t.integer :person_id, null: false
      t.integer :group_id, null: false
      t.timestamps
    end
  end
end
