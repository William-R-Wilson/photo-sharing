class AddColumnsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :admin, :boolean
    add_column :users, :authorized, :boolean
  end
end
