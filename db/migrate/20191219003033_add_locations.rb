class AddLocations < ActiveRecord::Migration[6.0]
  def change
    add_column :groups, :location, :string
    add_column :people, :location, :string
    add_column :events, :location, :string
    add_column :images, :location, :string
  end
end
