class CreateNoteEventTable < ActiveRecord::Migration[6.0]
  def change
    create_table :note_events do |t|
      t.integer :note_id
      t.integer :event_id
    end
  end
end
