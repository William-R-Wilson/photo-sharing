class GroupImage < ActiveRecord::Migration[6.0]
  def change
    create_table :group_images do |t|
      t.integer :group_id, null: false
      t.integer :image_id, null: false
    end
  end
end
