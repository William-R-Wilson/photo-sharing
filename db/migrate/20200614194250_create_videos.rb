class CreateVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :videos do |t|
      t.string :video
      t.string :location
      t.string :description
    end
  end
end
