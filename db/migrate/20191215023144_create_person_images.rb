class CreatePersonImages < ActiveRecord::Migration[6.0]
  def change
    create_table :person_images do |t|
      t.integer :person_id, null: false
      t.integer :image_id, null: false
    end
  end
end
