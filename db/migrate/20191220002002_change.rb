class Change < ActiveRecord::Migration[6.0]
  def change
    rename_table :group_persons, :group_people
  end
end
