class CreateNotePersonTable < ActiveRecord::Migration[6.0]
  def change
    create_table :note_people do |t|
      t.integer :note_id
      t.integer :person_id
    end
  end
end
