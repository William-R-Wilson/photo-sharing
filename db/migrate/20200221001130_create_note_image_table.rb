class CreateNoteImageTable < ActiveRecord::Migration[6.0]
  def change
    create_table :note_images do |t|
      t.integer :note_id
      t.integer :image_id
    end
  end
end
