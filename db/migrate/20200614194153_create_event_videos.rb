class CreateEventVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :event_videos do |t|
      t.integer :event_id
      t.integer :video_id
    end
  end
end
