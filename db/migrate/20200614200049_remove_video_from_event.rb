class RemoveVideoFromEvent < ActiveRecord::Migration[6.0]
  def change
    remove_column :events, :video, :string
  end
end
