class UserColumnsNotNull < ActiveRecord::Migration[6.0]
  def change
    change_column_null :users, :admin, false, false
    change_column_null :users, :authorized, false, false
  end
end
