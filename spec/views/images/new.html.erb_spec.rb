require 'rails_helper'

RSpec.describe "images/new", type: :view do
  before(:each) do
    assign(:image, Image.new(
      :description => "MyText",
      :image => "MyString"
    ))
  end

  it "renders new image form" do
    render

    assert_select "form[action=?][method=?]", images_path, "post" do

      assert_select "textarea[name=?]", "image[description]"

      assert_select "input[name=?]", "image[image]"
    end
  end
end
