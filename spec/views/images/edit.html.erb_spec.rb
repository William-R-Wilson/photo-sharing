require 'rails_helper'

RSpec.describe "images/edit", type: :view do
  before(:each) do
    @image = assign(:image, Image.create!(
      :description => "MyText",
      :image => "MyString"
    ))
  end

  it "renders the edit image form" do
    render

    assert_select "form[action=?][method=?]", image_path(@image), "post" do

      assert_select "textarea[name=?]", "image[description]"

      assert_select "input[name=?]", "image[image]"
    end
  end
end
