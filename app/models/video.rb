class Video< ApplicationRecord

  mount_uploader :video, VideoUploader

  has_one :event_image
  has_one :event, through: :event_image

end
