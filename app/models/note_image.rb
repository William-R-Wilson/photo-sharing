class NoteImage < ApplicationRecord

  belongs_to :note
  belongs_to :image

end
