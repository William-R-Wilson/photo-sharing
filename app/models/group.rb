class Group < ApplicationRecord

  has_many :group_people
  has_many :people, through: :group_people
  has_many :group_images
  has_many :images, through: :group_images
  has_many :note_groups
  has_many :notes, through: :note_groups
end
