class Event < ApplicationRecord

  has_many :event_videos
  has_many :videos, through: :event_videos
  has_many :person_events
  has_many :people, through: :person_events
  has_many :event_images
  has_many :images, through: :event_images
  has_many :note_events
  has_many :notes, through: :note_events

  accepts_nested_attributes_for :images
end
