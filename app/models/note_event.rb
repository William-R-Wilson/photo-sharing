class NoteEvent < ApplicationRecord

  belongs_to :event
  belongs_to :note

end
