class Note < ApplicationRecord

  belongs_to :note_event, optional: true
  belongs_to :note_group, optional: true
  belongs_to :note_image, optional: true
  belongs_to :note_person, optional: true

end
