class Person < ApplicationRecord

  has_many :person_events
  has_many :events, through: :person_events
  has_many :person_images
  has_many :images, through: :person_images
  has_many :group_people
  has_many :groups, through: :group_people
  has_many :note_people
  has_many :notes, through: :note_people

  def full_name
    first_name.to_s + " " + last_name.to_s
  end

end
