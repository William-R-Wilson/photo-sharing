class GroupImage < ApplicationRecord

  belongs_to :group
  belongs_to :image

end
