class Image < ApplicationRecord

  mount_uploader :image, ImageUploader

  has_many :person_images
  has_many :people, through: :person_images
  has_one :event_image
  has_one :event, through: :event_image
  has_many :group_images
  has_many :groups, through: :group_images
  has_many :note_images
  has_many :notes, through: :note_images

end
