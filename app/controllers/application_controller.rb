class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  before_action :authorize_user, except: :unauthorized

  def authorize_user
    unless current_user.nil?
      if !current_user.authorized
        redirect_to unauthorized_path, notice: "You are not yet authorized to use this application"
      end
    end
  end

  def add_note
    case params[:item_type]
      when  "person"
        person = Person.find(params[:item_id])
        note = Note.create(content: params[:content])
        person.notes << note
      when "group"
        group = Group.find(params[:item_id])
        note = Note.create(content: params[:content])
        group.notes << note
      when "event"
        event = Event.find(params[:item_id])
        note = Note.create(content: params[:content])
        event.notes << note
      when "image"
        image = Image.find(params[:item_id])
        note = Note.create(content: params[:content])
        image.notes << note
      end
    redirect_back(fallback_location: root_url)
  end


end
