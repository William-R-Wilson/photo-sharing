class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @people = Person.all.order(last_name: :asc).page(params[:page])
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @photo_arr = @person.images.each_slice(4).to_a
  end

  # GET /people/new
  def new
    @person = Person.new
    @groups_for_select = Group.all.order(name: :asc).map{ |group| [group.name, group.id] }
    @events_for_select = Event.all.order(name: :asc).map{ |event| [event.name, event.id] }
  end

  # GET /people/1/edit
  def edit
    @events_for_select = Event.all.order(name: :asc).map{ |event| [event.name, event.id] }
    @groups_for_select = Group.all.order(name: :asc).map{ |group| [group.name, group.id] }
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)

    respond_to do |format|
      if @person.save
        process_linking_params
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        process_linking_params
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'Person was successfully deleted' }
      format.json { head :no_content }
    end
  end

  def remove_linked_image
    @person = Person.find(params[:id])
    photo = Image.find(params[:photo_id])
    if @person.images.delete(photo)
      redirect_to @person, notice: "Linked image successfully removed"
    else
      redirect_to @person, notice: "Could not remove linked image"
    end
  end

  def remove_linked_group
    @person = Person.find(params[:id])
    group = Group.find(params[:group_id])
    if @person.groups.delete(group)
      redirect_to @person, notice: "Linked group successfully removed"
    else
      redirect_to @person, notice: "Could not remove linked group"
    end
  end

  def remove_linked_event
    event = Event.find(params[:event_id])
    if @person.events.delete(event)
      redirect_to @person, notice: "Linked event successfully removed"
    else
      redirect_to @person, notice: "Could not remove linked event"
    end
  end

  def search
    search_params = params[:search_params].downcase
    if search_params.split.length == 1
      @people = Person.where("lower(first_name) LIKE :search", search: "%#{search_params}%").or(
                            Person.where("lower(last_name) LIKE :search", search: "%#{search_params}%"))
    else
      first_name = "%#{search_params.split.first}%"
      last_name = "%#{search_params.split.last}%"
      @people = Person.where("lower(first_name) LIKE :first_name AND lower(last_name) LIKE :last_name", last_name: last_name, first_name: first_name).or(
                Person.where("lower(last_name) LIKE :first_name AND lower(first_name) LIKE :last_name", last_name: last_name, first_name: first_name))

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    def process_linking_params
        if params[:person_images].present?
          params[:person_images].each do |img|
            image = Image.new(description: img.original_filename, image: img.tempfile)
            if image.save
              @person.images << image
            end
          end
        end
        if params[:person][:link_group].present?
          group = Group.find(params[:person][:link_group])
          @person.groups << group
        end
        if params[:person][:link_event].present?
          event = Event.find(params[:person][:link_event])
          @person.events << event
        end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:first_name, :last_name, :location)
    end
end
