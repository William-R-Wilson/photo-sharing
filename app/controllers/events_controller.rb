class EventsController < ApplicationController

  def index
    @events = Event.all.order(date: :asc).page(params[:page])
  end

  def show
    @event = Event.find(params[:id])
    @photo_arr = @event.images.each_slice(4).to_a
    @video_arr = @event.videos.to_a
end

  def new
    @event = Event.new
    @people_for_select = Person.all.map{ |person| ["#{person.first_name} #{person.last_name}", person.id] }
  end

  def create
    @event = Event.new(event_params)
    respond_to do |format|
      if @event.save
        process_linking_params
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @event = Event.find(params[:id])
    @people_for_select = Person.all.map{ |person| ["#{person.first_name} #{person.last_name}", person.id] }
  end

  def update
    @event = Event.find(params[:id])
    respond_to do |format|
      if @event.update(event_params)
      process_linking_params
        # @event.people << Person.find(params[:image][:link_person])
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def remove_linked_group
    @event = Event.find(params[:id])
    group = Group.find(params[:group_id])
    if @evnet.groups.delete(group)
      redirect_to @event, notice: "Linked group successfully removed"
    else
      redirect_to @event, notice: "Could not remove linked group"
    end
  end


  def remove_linked_person
    @event = Event.find(params[:id])
    person = Person.find(params[:person_id])
    if @event.people.delete(person)
      redirect_to @event, notice: "Linked person successfully removed"
    else
      redirect_to @event, notice: "Could not remove linked_person"
    end
  end

  def remove_linked_image
    @event = Event.find(params[:id])
    photo = Image.find(params[:photo_id])
    if @event.images.delete(photo)
      redirect_to @event, notice: "Linked image successfully removed"
    else
      redirect_to @event, notice: "Could not remove linked image"
    end
  end

  private

    def event_params
      params.require(:event).permit(:name, :description, :date, :location )
    end

    def process_linking_params
        if params[:event_images].present?
          params[:event_images].each do |img|
            image = Image.new(description: img.original_filename, image: img.tempfile)
            if image.save
              @event.images << image
            end
          end
        end
        if params[:event_videos].present?
          params[:event_videos].each do |video|
            video = Video.new(description: video.original_filename, video: video.tempfile)
            if video.save
              @event.videos << video
            end
          end
        end
        if params[:event_people].present?
          params[:event_people].each do |person|
            unless person.blank?
              person = Person.find(person)
              @event.people << person unless @event.people.include?(person)
            end
          end
        end

        #if params[:event][:link_group].present?
       #   group = Group.find(params[:person][:link_group])
       #   @person.groups << group
       # end
       # if params[:person][:link_event].present?
       #   event = Event.find(params[:person][:link_event])
       #   @person.events << event
       # end
    end

end
