class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :full_size, :edit, :update, :destroy]

  # GET /images
  # GET /images.json
  def index
    @images = Image.all.order(created_at: :asc).page(params[:page])
  end

  # GET /images/1
  # GET /images/1.json
  def show
  end

  def full_size
  end

  # GET /images/new
  def new
    @image = Image.new
    @people_for_select = Person.all.order(last_name: :asc, first_name: :asc).map{ |person| [person.full_name, person.id] }
    @events_for_select = Event.all.order(name: :asc).map{ |event| [event.name, event.id] }
    @groups_for_select = Group.all.order(name: :asc).map{ |group| [group.name, group.id] }
  end

  # GET /images/1/edit
  def edit
    @people_for_select = Person.all.order(last_name: :asc, first_name: :asc).map{ |person| [person.full_name, person.id] }
    @events_for_select = Event.all.order(name: :asc).map{ |event| [event.name, event.id] }
    @groups_for_select = Group.all.order(name: :asc).map{ |group| [group.name, group.id] }
  end

  # POST /images
  # POST /images.json
  def create
    @image = Image.new(image_params)

    respond_to do |format|
      if @image.save
        process_linking_params
        format.html { redirect_to @image, notice: 'Image was successfully created.' }
        format.json { render :show, status: :created, location: @image }
      else
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /images/1
  # PATCH/PUT /images/1.json
  def update
    respond_to do |format|
      if @image.update(image_params)
        process_linking_params

        format.html { redirect_to @image, notice: 'Image was successfully updated.' }
        format.json { render :show, status: :ok, location: @image }
      else
        format.html { render :edit }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
    @image.destroy
    respond_to do |format|
      format.html { redirect_to images_url, notice: 'Image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove_linked_person
      @image = Image.find(params[:id])
      person = Person.find(params[:person_id])
      if @image.people.delete(person)
        redirect_to @image, notice: "Linked person successfully removed"
      else
        redirect_to @image, notice: "Could not remove linked person"
      end
  end

  def remove_linked_group
    @image = Image.find(params[:id])
    group = Group.find(params[:group_id])
    if @image.groups.delete(group)
      redirect_to @image, notice: "Linked group successfully removed"
    else
      redirect_to @image, notice: "Could not remove linked group"
    end
  end

  def remove_linked_event
    @image = Image.find(params[:id])
    if @image.event_image.delete
      redirect_to @image, notice: "Linked event successfully removed"
    else
      redirect_to @image, notice: "Could not remove linked event"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    def process_linking_params
        if !params[:image][:link_event].empty?
          @image.event = Event.find(params[:image][:link_event])
        end
        if !params[:image][:link_person].empty?
          params[:image][:link_person].each do |imp|
            unless imp.blank?
              person = Person.find(imp)
              @image.people << person unless @image.people.include?(person)
              unless @image.event.nil?
                @image.event.people << person unless @image.event.people.include?(person)
              end
            end
          end
        end

        if !params[:image][:link_group].empty?
          params[:image][:link_group].each do |gp|
            @image.groups << Group.find(gp) unless gp.blank?
          end
        end
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def image_params
      params.require(:image).permit(:description, :image, :location)
    end
end
