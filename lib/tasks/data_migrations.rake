namespace :data_migrations do 

  task test_rake: :environment do 
    puts "dammit bobby"
  end


  task link_people_in_event_photos_to_event: :environment do 
    Event.all.each do |event|
      event.images.each do |image|
        image.people.each do |person|
          unless event.people.include?(person)
            event.people << person
          end
        end
      end
    end 
  end

end
