Rails.application.routes.draw do

  root to: "people#index"

  post :add_note, to: "application#add_note"

  devise_for :users

  resources :users, only: :index
  get 'unauthorized', to:  'users#unauthorized', as: :unauthorized

  resources :groups

  resources :images do
    member do
      get 'full_size', to: 'images#full_size'
      get 'remove_linked_event', to: 'images#remove_linked_event'
      get 'remove_linked_person', to: 'images#remove_linked_person'
      get 'remove_linked_group', to: 'images#remove_linked_group'
    end
  end

  resources :people do
    member do
      get 'remove_linked_image', to: 'people#remove_linked_image'
      get 'remove_linked_group', to: 'people#remove_linked_group'
      get 'remove_linked_event', to: 'people#remove_linked_event'
    end
    collection do
      get 'search', to: "people#search"
    end
  end

  resources :events do
    member do
      get 'remove_linked_image', to: 'events#remove_linked_image'
      get 'remove_linked_group', to: 'events#remove_linked_group'
      get 'remove_linked_event', to: 'events#remove_linked_event'
      get 'remove_linked_person', to: 'events#remove_linked_person'
    end
  end


# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

end
